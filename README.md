# React Forms and Validations ( inspired by [Hayneedle](https://www.hayneedle.com/account/login) )
**Built with Create React App, Typescript, MaterialUI, & React Router**
by [Alex Segers](https://gitlab.mynisum.com/asegers)

**[LIVE DEMO](https://asegers.github.io/react-ts-materialui-routing-demo)**