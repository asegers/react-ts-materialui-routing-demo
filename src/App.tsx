import { Box } from '@material-ui/core'

import { StyleProvider } from 'styles'
import { Router } from 'routes'
import { Header, Footer } from 'components'

const App = () => (
    <StyleProvider>
      <Box display="flex" flexDirection="column" minHeight="100vh">
        <Header/>
        <Box flex={1}>
          <Router/>   
        </Box>
        <Footer/>
      </Box>
  </StyleProvider>
)

export default App
