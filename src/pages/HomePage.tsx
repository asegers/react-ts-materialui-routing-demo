import { Box, Divider } from '@material-ui/core'

import { LoginForm, SignUpForm } from 'components'

export const HomePage = () => {
    return (
        <Box display="flex" justifyContent="center" m={5}>
            <LoginForm/>
            <Box mx={3}><Divider orientation="vertical" /></Box>
            <SignUpForm/>
        </Box>
    )
}
