import React from 'react'
import {BrowserRouter, Redirect, Route, Switch} from 'react-router-dom'
import {HomePage} from 'pages'

const Router: React.FC = () => (
    <BrowserRouter basename={process.env.PUBLIC_URL}>
        <Switch>
            <Route exact path="/account/login" component={HomePage} />
            <Route path="/"><Redirect to="/account/login" /></Route>
        </Switch>
    </BrowserRouter>
)

export default Router;