import { useState } from 'react'
import { 
    Button, 
    Checkbox, 
    Grid, 
    FormGroup, 
    FormControlLabel,
    Typography 
} from '@material-ui/core'
import { 
    ValidatorForm, 
    TextValidator,
} from 'react-material-ui-form-validator';

const SignUpForm = () => {
    const [formData, setFormData] = useState({ firstName: '', lastName: '', email: '', password: '', sendDeals: true })
    const {firstName, lastName, email, password, sendDeals } = formData

    const handleTextChange = e => {
        const { currentTarget: { name, value } } = e;
        setFormData(prev => ({ ...prev, [name]: value }));
    }

    const handleCheckboxClick= e => {
        const { currentTarget: { name } } = e;
        setFormData(prev => ({ ...prev, [name]: !prev[name] }));
    }

    const handleFormSubmit = e => {
        e.preventDefault();
        alert(JSON.stringify(formData, null, 3));
    }

    return (
        <ValidatorForm onSubmit={handleFormSubmit}>
            <Grid container direction="column" spacing="3" alignItems="flex-start">
                <Grid item><Typography variant="h5">Create Account</Typography></Grid>
                <Grid item>
                    <TextValidator
                        label="First Name"
                        onChange={handleTextChange}
                        name="firstName"
                        value={firstName}
                        validators={['required']}
                        errorMessages={['Please enter a first name']}
                        variant="filled" size="small"
                    />
                </Grid>
                <Grid item>
                    <TextValidator
                        label="Last Name"
                        onChange={handleTextChange}
                        name="lastName"
                        value={lastName}
                        validators={['required']}
                        errorMessages={['Please enter a last name']}
                        variant="filled" size="small"
                    />
                </Grid>
                <Grid item>
                    <TextValidator
                        type="email"
                        label="Email Address"
                        onChange={handleTextChange}
                        name="email"
                        value={email}
                        validators={[
                            'required', 
                            'isEmail'
                        ]}
                        errorMessages={[
                            'Please enter an email', 
                            'Email address must be a valid email'
                        ]}
                        variant="filled" size="small"
                    />
                </Grid>
                <Grid item>
                    <TextValidator
                        type="password"
                        label="Password"
                        onChange={handleTextChange}
                        name="password"
                        value={password}
                        validators={[
                            'required', 
                            'matchRegexp:^[!@#$%^&]$'
                        ]}
                        errorMessages={[
                            'Please enter a password', 
                            'Must have 1 special character (!@#$%^&)'
                        ]}
                        variant="filled" size="small"
                    />
                </Grid>
                <Grid item>
                    <FormGroup style={{ flexDirection: "row" }}>
                        <FormControlLabel 
                            control={<Checkbox 
                                name="sendDeals" 
                                onChange={handleCheckboxClick}
                                checked={sendDeals} 
                            />} 
                            label="Send me exclusive deals" 
                        />
                    </FormGroup>
                </Grid>
                <Grid item>
                    <Button variant="outlined" type="submit">Create an Account</Button>
                </Grid>
            </Grid>
        </ValidatorForm>
    );
    
}

export default SignUpForm;
