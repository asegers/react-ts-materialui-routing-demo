import { 
    AppBar,
    Box,
    Icon,
    IconButton,
    InputBase,
    Toolbar, 
    Typography,
} from '@material-ui/core'
import { 
    fade, 
    makeStyles 
} from '@material-ui/core/styles';
import { 
    ShoppingCart as CartIcon, 
    Person as PersonIcon, 
    Favorite as FavoriteIcon, 
    ContactSupport as SupportIcon,
    Search as SearchIcon
} from '@material-ui/icons'
import logoSrc from 'logo.svg'


const useStyles = makeStyles((theme) => ({
    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.white, 0.15),
      '&:hover': {
        backgroundColor: fade(theme.palette.common.white, 0.25),
      },
      marginRight: theme.spacing(2),
      marginLeft: 0,
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        margin: "0 auto",
        width: 'auto',
      },
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    inputRoot: {
      color: 'inherit',
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: '40vw',
      },
    }
  }));


const Logo = () => (
    <Box display="flex" justifyItems="center">
        <Typography variant="h4">StrawStack</Typography>
        <Icon fontSize="large"><img src={logoSrc} alt="Logo" height="100%"/></Icon> 
    </Box>
)

const Searchbar = () => {
    const classes = useStyles();
    return (
        <Box className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase 
                placeholder="Search ..." 
                classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput,
                }}
            />
        </Box>
    )
}

const IconMenu = () => (
    <Box display="flex">
        <IconButton><SupportIcon/></IconButton>
        <IconButton><CartIcon/></IconButton>
        <IconButton><FavoriteIcon/></IconButton>
        <IconButton><PersonIcon/></IconButton>
    </Box>
)

const Header = () => {
    return (
        <AppBar position="sticky">
            <Toolbar>
                <Logo/>
                <Searchbar/>
                <IconMenu/>
            </Toolbar>
        </AppBar>
    );
}

export default Header;
